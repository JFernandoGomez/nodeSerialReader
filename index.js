"use strict";

var SerialPortLib = require("serialport");
var request 	  = require("request");
var storage 	  = require('node-persist');
var config 		  = require("./config");
var myBuffer 	  = require('buffer/').Buffer;


var os = require('os');
var ifaces = os.networkInterfaces();



var mySerialPort  = SerialPortLib.SerialPort;
var timeRefresh   = config.timeRefresh * 60 * 1000; // in minutes
var destinationServer = config.destinationServer;
var sp;
// where to save port info
var actualPort;
var counter;
var portAvailable = false;
var myString = "";
var innerCounter = 0;

// gets ur computer ready for the module
storage.initSync();

var addr;

var startSerialRead = function( ){
	// if theres a device configured
	if(typeof config.device.manufacturer == "string" && config.device.manufacturer.length > 0){
		
		console.log("init SerialReader app");
		
		watchSerialPort();

		sendSerialInfo();

	}else{
		console.error("No device configured");
	}

}

var watchSerialPort = function(){

	getAvailablePort().then(
		// success
		function(portInfo){
			actualPort = portInfo;
			
			// initialize the port with the given comName
			sp = new mySerialPort( actualPort.comName ,{

				// baudrate defined in config.js
				baudrate: config.serial.baudrate,
				dataBits: config.serial.dataBits,
				stopBits: config.serial.stopBits,
				parity: config.serial.parity,
				parser: config.parser ? SerialPortLib.parsers.readline("B") : ''

			});


			// listen to port
			sp.on('open', function(error){
				if ( error ) {
					console.log('failed to open: '+error);
				} else {	
					// port open

					console.log('Serial Port Opened');
					sp.on('data', function(data){

						myString = data.toString('ascii');
						console.log(myString);

						if(config.parser){
							counter = myString.replace(/A/g, "");
							// handle port info
						  	
						  	counter = parseInt(counter);
						  	// save for future use
						  	storage.setItem('counter', counter);
						  	console.log("-- value: "+counter);

						}else{
							// in case its buffering info

							if(myString == "A"){
								innerCounter = "";
							}else if( myString == "B"){
									// handle port info
								  	
								  	// save for future use
								  	// replace special housing characters
								  	
								  	console.log("counter when B: "+innerCounter);
								  	counter = parseInt(innerCounter);
								  	storage.setItem('counter', counter);
								  	console.log("-- value: "+counter);							
							}else{
								innerCounter = innerCounter + myString;
							}

						}
						


						
						
					});

				}
			});

			sp.on('close', function(data){
				portAvailable = false;
				console.warn('Serial Port Closed!');
				console.warn('Waiting for new connection - last data will be sent untill new connection.');
				// console.log(' waitSerialConnection() ');
				waitSerialConnection();
			});



		},
		// error
		function(){
			console.warn("No ports, waiting for ports each 10 sec. ");
			
			waitSerialConnection();
		}
	);
}

var getAvailablePort = function (){

	// look for Desired device in available ports
	return new Promise(function(resolve, reject){

		SerialPortLib.list(function (err, ports) {
			portAvailable = false;
			console.log("-- looking for ports");
			
			ports.forEach(function(port, i) {
				if(portAvailable) return;
				// console.log(port);
				
				if( typeof port.manufacturer != "undefined"){

					if( port.manufacturer.indexOf(config.device.name) >= 0 ){

						//console.log(port.manufacturer.indexOf(config.device.name));
						console.log("port located: "+port.comName);
						resolve(port);
						portAvailable = true;
						return;
					}	
				
				}
				
				// if we reached the end of the ports
				if( i + 1 == ports.length ){
					console.log("-- No ports portAvailable");
					reject();
				}
			});
		});	

	});

}

var waitSerialConnection = function(){
	var secs 	= 10 * 1000;
	var timer 	= setInterval(function(){
		if(portAvailable == false){
			watchSerialPort();
		}
		clearInterval(timer);
	},secs)
}

var sendSerialInfo = function(){

	//console.log(typeof counter);	
	if(typeof counter == 'undefined'){
		storage.getItem('counter') ?  counter = storage.getItem('counter') : counter = 0;
		console.warn("WARNING: No 'counter value' initialized, going to last persistant data: "+counter);
	}

	setInterval(function(){

		//console.log("portAvailable: "+portAvailable);
		//console.log('http://www.plazadelsol.com/webservices/serial.php?contador='+ counter +'&status=true');
		request(destinationServer.url+'?contador='+counter+'&status='+portAvailable+'&localIp='+addr, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		    console.log('Response from server:'); 
		    console.log(body);
		    // Show the HTML for the Google homepage. 
		  }
		})
	},timeRefresh);

}

var getIp = function(){
	var address;
	var ip = Object.keys(ifaces).forEach(function (ifname) {
	  var alias = 0;

	  ifaces[ifname].forEach(function (iface) {
	    if ('IPv4' !== iface.family || iface.internal !== false) {
	      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
	      return;
	    }

	    if (alias >= 1) {
	      // this single interface has multiple ipv4 addresses
	      // console.log(ifname + ':' + alias, iface.address);
	      address =  iface.address;
	    } else {
	      // this interface has only one ipv4 adress
	      // console.log(ifname, iface.address);
	      address = iface.address;
	    }
	    ++alias;
	    
	  });
	  
	});

	return address;
}

addr = getIp()
console.log('local ip: '+ addr);

startSerialRead();


