var config = {

	"device": {
		"name" 	: "Arduino",
		"manufacturer" : "Arduino (www.arduino.cc)"
		// "name" 	: "Prolific",
		// "manufacturer" : "Prolific_Technology_Inc."
	},
	"serial":{
		"baudrate" : 9600,
		"string": "B",
		"dataBits": 8,
		"stopBits": 1,
		"parity": 'none'
	},

	"timeRefresh" : 0.5 ,// minutos

	"destinationServer": {
		"url": "http://plazadelsol.com/webservices/serial.php",
		"method": "get"
	},
	"parser":true
};


module.exports = config;

